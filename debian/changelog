crdt-el (0.3.5-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:51:53 +0900

crdt-el (0.3.5-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:36:27 +0900

crdt-el (0.3.5-2) unstable; urgency=medium

  * Team upload
  * d/watch
    - Add a watch file
  * d/control
    - Bump Standards-Version to 4.6.2
    - Remove obsolete version constraint for `dh-elpa` build-dep
    - White-space fix
  * d/gbp.conf
    - Define branches and enable `pristine-tar`
  * d/.gitignore
    - Ignore packaging and editing artefacts
  * d/elpa
    - Fix installation of upstream crdt-pkg.el by replacing out-of-date
      version in path with `${env:DEB_VERSION_UPSTREAM}`
  * d/rules
    - Export `DEB_VERSION_UPSTREAM` for d/elpa
    - Make build verbose unless the `terse` build option is used
  * d/copyright
    - Update upstream notice
    - Use https URL for `Format:`

 -- Jeremy Sowden <jeremy@azazel.net>  Sat, 25 Nov 2023 21:02:25 +0000

crdt-el (0.3.5-1) unstable; urgency=medium

  * New upstream release
  * debian/control: cleanups: bump debhelper-compat to 13, add R³,
    fix Vcs-Browser, add myself as uploader as replacement for David
  * Drop obsolete patch to add autoload cookies
  * Add docs

 -- Martin <debacle@debian.org>  Fri, 24 Nov 2023 19:15:48 +0000

crdt-el (0.0~20210526-3) unstable; urgency=medium

  * Source only upload

 -- David Bremner <bremner@debian.org>  Sun, 22 Jan 2023 13:05:45 -0400

crdt-el (0.0~20210526-2) unstable; urgency=medium

  * Initial upload (Closes: #978721)
  * Upstream git commit b5a8a28cf6

 -- David Bremner <bremner@debian.org>  Sat, 03 Jul 2021 12:15:36 -0300
